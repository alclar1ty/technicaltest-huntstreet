<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Designer;

class DesignerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/data/designer_list.csv"), "r");
  
        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {
            Designer::create([
                "name" => $data['2'],
            ]); 
        }
   
        fclose($csvFile);
    }
}
