<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\InvitationStatus;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        InvitationStatus::create([
	        'name'  => 'New'
		]);

		InvitationStatus::create([
	        'name'  => 'Registered'
		]);
    }
}
