<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationDesignersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_designers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('invitation_id')->unsigned();
            $table->foreign('invitation_id')->references('id')->on('invitations');
            $table->foreignId('designer_id')->unsigned();
            $table->foreign('designer_id')->references('id')->on('designers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_designers');
    }
}
