<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Models\Invitation;
use Validator;
use Carbon\Carbon;

class InvitationController extends Controller
{
    public function index($unique_link)
    {
        $invitation = Invitation::where('unique_link', $unique_link)->first();
        return $invitation->toJson($invitation);
    }

    public function store(Request $request){
    	$validator = \Validator::make($request->all(), [
            'email' => 'required|email|unique:invitations',
        ]);

        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        else{
        	$invitationObj = new Invitation();
            $invitationObj->fill($request->only(
                "email",
            ));
            $encryptResult = Crypt::encryptString($request->email);
            $invitationObj->unique_link = substr($encryptResult, 0, 8).substr($encryptResult, -8, 8);
            $invitationObj->status_id = 1;
            $invitationObj->save();

            //kirim emmail 1 jam
            $this->dispatch((new \App\Jobs\SendEmailJob($invitationObj->email, $invitationObj->unique_link))->delay(Carbon::now()->addSeconds(3600)));

            return response()->json(['success' => $invitationObj]);
        }
    }

    public function destroy(Request $request){
    	Invitation::find($request->id)->delete();
        return redirect()->back();
    }
}
