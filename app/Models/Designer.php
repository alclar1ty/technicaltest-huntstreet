<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Designer extends Model
{
    protected $fillable = [
        'name'
    ];

    public function invitation()
    {
        return $this->hasMany('App\Models\InvitationDesigner');
    }
}
