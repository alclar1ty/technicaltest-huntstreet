<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
	protected $fillable = [
        'unique_link', 'name', 'email', 'birth_date', 'gender', 'status_id'
    ];


    public function status()
    {
        return $this->belongsTo('App\Models\InvitationStatus', 'status_id', 'id');
    }

    public function designer()
    {
        return $this->hasMany('App\Models\InvitationDesigner');
    }
}
