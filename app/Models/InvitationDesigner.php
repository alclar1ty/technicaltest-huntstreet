<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationDesigner extends Model
{
	protected $fillable = [
        'invitation_id', 'designer_id'
    ];

    public function invitation()
    {
        return $this->belongsTo('App\Models\Invitation', 'invitation_id', 'id');
    }

    public function designer()
    {
        return $this->belongsTo('App\Models\Designer', 'designer_id', 'id');
    }
}
