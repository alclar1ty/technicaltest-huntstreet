<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationStatus extends Model
{
	protected $fillable = [
        'name'
    ];

    public function invitation()
    {
        return $this->hasMany('App\Models\Invitation');
    }
}
