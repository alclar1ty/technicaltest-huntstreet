<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvitaionMail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $send_mail;
    protected $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($send_mail, $url)
    {
        $this->send_mail = $send_mail;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = $this->url;
        $send_mail = $this->send_mail;

        Mail::send('email.invitation', compact('url'), function($message) use ($send_mail) {
            $message->to($send_mail, '-')->subject('HUNTBAZAAR INVITATION');
            $message->from('no-reply@huntstreet.com', 'HUNTSTREET');
        });
    }
}
