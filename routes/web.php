<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('admin');
});
Route::get('/invitation', function () {
    return view('app');
})->name('invitation-link');

Auth::routes();
Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])
	->name('admin');
Route::post('/store', [App\Http\Controllers\InvitationController::class, 'store'])
	->middleware('auth')
    ->name('store_invitation');
Route::post('/destroy', [App\Http\Controllers\InvitationController::class, 'destroy'])
	->middleware('auth')
    ->name('delete_invitation');
