@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>

{{-- LIST INVITATION --}}
<div class="container pt-4">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center h3"><strong>List Invitation</strong></div>

                <div class="card-body">
                    <div class="col-12 pb-3 d-flex justify-content-end margin-auto">
                        <button class="btn btn-primary"
                                data-toggle="modal"
                                href="#addModal">Add Invitation</button>
                    </div>
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th scope="col" class="col-3">Email</th>
                                <th scope="col" class="col-6">Link</th>
                                <th scope="col" class="col-2">Status</th>
                                <th colspan="2" scope="col" class="col-1">View/Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($invitations as $inv)
                                <tr>
                                    <td style="vertical-align: middle;">{{ $inv->email }}</td>
                                    <td style="vertical-align: middle;">
                                        <a href="{{ route('invitation-link', ['unique_link' => $inv->unique_link]) }}" target="_blank">
                                            {{ route('invitation-link', ['unique_link' => $inv->unique_link]) }}
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;">{{ $inv->status['name'] }}</td>
                                    <td><a data-toggle="modal"
                                            href="#viewModal"
                                            onclick="viewModal(this)"
                                            data-id="{{ $inv["id"] }}">
                                                <i class="mdi mdi-eye" style="font-size: 1.6em; color: #337ab7"></i>
                                        </a>
                                    </td>
                                    <td><a data-toggle="modal"
                                            href="#deleteModal"
                                            onclick="submitDelete(this)"
                                            data-id="{{ $inv["id"] }}">
                                                <i class="mdi mdi-delete" style="font-size: 1.6em; color: #d9534f"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            @if(sizeof($invitations) < 1)
                                <tr>
                                    <td colspan="5" scope="col" class="text-center h5">No Data</td>
                                </tr>
                            @endif
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add -->
<div class="modal fade"
    id="addModal"
    tabindex="-1"
    role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Add New Invitation
                </h5>
                <button type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="actionAdd" action="{{ route('store_invitation') }}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Invitation Email</label>
                        <input type="email"
                            class="form-control"
                            id="add-email"
                            name="email"
                            placeholder="Invitation Email"
                            required />
                        <small id="alert-txt" class="d-none"></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="btn-submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Add -->

<!-- Modal Delete -->
<div class="modal fade"
    id="deleteModal"
    tabindex="-1"
    role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="text-center">
                    Are you sure to delete this invitation?
                </h5>
            </div>
            <div class="modal-footer">
                <form id="frmDelete"
                    method="post"
                    action="{{ route('delete_invitation') }}">
                    @csrf
                    <input type="hidden" name="id" id="id-delete" />
                    <button type="submit"
                        class="btn btn-danger mr-2">
                        Yes
                    </button>
                </form>
                <button class="btn btn-primary">No</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Delete -->
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $("#actionAdd").on("submit", function (e) {
            e.preventDefault();
            var frmAdd = new FormData(document.getElementById("actionAdd"));
            frmAdd.enctype = "multipart/form-data";
            var URLNya = $("#actionAdd").attr('action');

            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler, false);
            ajax.addEventListener("load", completeHandler, false);
            ajax.addEventListener("error", errorHandler, false);
            ajax.open("POST", URLNya);
            ajax.setRequestHeader("X-CSRF-TOKEN",$('meta[name="csrf-token"]').attr('content'));
            ajax.send(frmAdd);
        });
        function progressHandler(event){
            document.getElementById("btn-submit").innerHTML = "Loading...";
        }
        function completeHandler(event){
            var hasil = JSON.parse(event.target.responseText);
            if(hasil['errors'] != null){
                $('#add-email').addClass('alert');
                $('#alert-txt').addClass('alert');
                $('#alert-txt').text(hasil['errors'][0]);
            }
            else{
                alert("New Invitation has been Successfully Added");
                window.location.reload();
            }

            document.getElementById("btn-submit").innerHTML = "Submit";
        }
        function errorHandler(event){
            document.getElementById("btn-submit").innerHTML = "Loading...";
        }

        $('#add-email').change(function(){
            $('#add-email').removeClass('alert');
            $('#alert-txt').removeClass('alert');
        });
    });

    function submitDelete(e) {
        document.getElementById("id-delete").value = e.dataset.id;
    }
</script>
@endsection